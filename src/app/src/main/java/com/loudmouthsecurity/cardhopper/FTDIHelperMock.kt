package com.loudmouthsecurity.cardhopper

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.ftdi.j2xx.D2xxManager
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlin.math.min

class FTDIHelperMock(private val deviceUARTContext: Context, private val ftdid2xx: D2xxManager): FTDIHelper(deviceUARTContext, ftdid2xx) {


    override fun init() {
        return
    }


    override fun createDeviceList() {
        return
    }

    override fun disconnectFunction() {
       return
    }

    override fun connectFunction() {
       return
    }

    override fun isOpen(): Boolean {
        return true
    }

    override fun setConfig(baud: Int, dBits: Byte, sBits: Byte, par: Byte, flowControl: Byte) {
       return
    }

    override fun startReadingMessages() = CoroutineScope(Dispatchers.IO).launch {

    }

    override suspend fun readMessage(timeout: Long): ByteArray {
        delay(timeout)
        return "Sample Data".toByteArray()
    }

    override suspend fun sendMessage(message: ByteArray) {
        println("Message sent: ${String(message)}")
    }

    override fun getReadTimeout(): Long {
        return 5000
    }
}