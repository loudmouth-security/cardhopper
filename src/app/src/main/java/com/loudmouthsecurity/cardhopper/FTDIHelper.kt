package com.loudmouthsecurity.cardhopper

import android.content.Context
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import com.ftdi.j2xx.D2xxManager
import com.ftdi.j2xx.FT_Device
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.coroutines.withTimeout
import kotlin.coroutines.cancellation.CancellationException
import kotlin.math.min

open class FTDIHelper(private val deviceUARTContext: Context, private val ftdid2xx: D2xxManager) {

    private var readData = ByteArray(readLength)
    private var readDataToText = CharArray(readLength)

    companion object {
        private const val readLength = 512
    }
    // original ///////////////////////////////
    var ftDev: FT_Device? = null
    var DevCount = -1
    var currentIndex = -1
    var openIndex = 0

    var iavailable = 0

    var uart_configured = false

    // Create a shared flow to emit messages
    private var messagesFlow: MutableSharedFlow<ByteArray>? = null

    private val ACK = "fe".toInt(16).toByte()

    private var readTimeout: Long = 5000L

    open fun init() {
        if (DevCount <= 0) {
            createDeviceList()
        }
        if (DevCount > 0) {
            connectFunction()
        } else {
            Toast.makeText(deviceUARTContext, "No devices attached", Toast.LENGTH_LONG)
                .show()
        }
    }

    open fun getReadTimeout(): Long {
        return readTimeout
    }


    fun notifyUSBDeviceAttach() {
        init()
    }

    fun notifyUSBDeviceDetach() {
        disconnectFunction()
    }

    open fun createDeviceList() {
        val tempDevCount = ftdid2xx.createDeviceInfoList(deviceUARTContext)
        if (tempDevCount > 0) {
            if (DevCount != tempDevCount) {
                DevCount = tempDevCount
                //updatePortNumberSelector();
            }
        } else {
            DevCount = -1
            currentIndex = -1
        }
    }

    open fun disconnectFunction() {
        DevCount = -1
        currentIndex = -1
        try {
            Thread.sleep(50)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
        if (ftDev != null) {
            synchronized(ftDev!!) {
                if (true == ftDev!!.isOpen) {
                    ftDev!!.close()
                }
            }
        }
    }

    open fun connectFunction() {
        val tmpProtNumber = openIndex + 1
        if (currentIndex != openIndex) {
            if (null == ftDev) {
                ftDev = ftdid2xx.openByIndex(deviceUARTContext, openIndex)
            } else {
                synchronized(ftDev!!) {
                    ftDev = ftdid2xx.openByIndex(deviceUARTContext, openIndex)
                }
            }
            uart_configured = false
        } else {
            Toast.makeText(
                deviceUARTContext,
                "Device port $tmpProtNumber is already opened", Toast.LENGTH_LONG
            ).show()
            return
        }
        if (ftDev == null) {
            Toast.makeText(
                deviceUARTContext,
                "open device port($tmpProtNumber) NG, ftDev == null", Toast.LENGTH_LONG
            ).show()
            return
        }
        if (true == ftDev!!.isOpen) {
            currentIndex = openIndex
            Toast.makeText(
                deviceUARTContext,
                "open device port($tmpProtNumber) OK",
                Toast.LENGTH_SHORT
            ).show()
        } else {
            Toast.makeText(
                deviceUARTContext,
                "open device port($tmpProtNumber) NG",
                Toast.LENGTH_LONG
            ).show()
            //Toast.makeText(DeviceUARTContext, "Need to get permission!", Toast.LENGTH_SHORT).show();
        }
    }

    open fun isOpen(): Boolean {
        return if (ftDev != null) {
            ftDev!!.isOpen
        } else {
            false
        }
    }

    open fun setConfig(baud: Int, dBits: Byte, sBits: Byte, par: Byte, flowControl: Byte) {
        var dataBits = dBits
        var stopBits = sBits
        var parity = par
        if (!ftDev!!.isOpen) {
            Log.e("j2xx", "SetConfig: device not open")
            return
        }

        // configure our port
        // reset to UART mode for 232 devices
        ftDev!!.setBitMode(0.toByte(), D2xxManager.FT_BITMODE_RESET)
        ftDev!!.setBaudRate(baud)
        dataBits = when (dataBits) {
            7.toByte() -> D2xxManager.FT_DATA_BITS_7
            8.toByte() -> D2xxManager.FT_DATA_BITS_8
            else -> D2xxManager.FT_DATA_BITS_8
        }
        stopBits = when (stopBits) {
            1.toByte() -> D2xxManager.FT_STOP_BITS_1
            2.toByte() -> D2xxManager.FT_STOP_BITS_2
            else -> D2xxManager.FT_STOP_BITS_1
        }
        parity = when (parity) {
            0.toByte() -> D2xxManager.FT_PARITY_NONE
            1.toByte() -> D2xxManager.FT_PARITY_ODD
            2.toByte() -> D2xxManager.FT_PARITY_EVEN
            3.toByte() -> D2xxManager.FT_PARITY_MARK
            4.toByte() -> D2xxManager.FT_PARITY_SPACE
            else -> D2xxManager.FT_PARITY_NONE
        }
        ftDev!!.setDataCharacteristics(dataBits, stopBits, parity)
        val flowCtrlSetting: Short
        flowCtrlSetting = when (flowControl) {
            0.toByte() -> D2xxManager.FT_FLOW_NONE
            1.toByte() -> D2xxManager.FT_FLOW_RTS_CTS
            2.toByte() -> D2xxManager.FT_FLOW_DTR_DSR
            3.toByte() -> D2xxManager.FT_FLOW_XON_XOFF
            else -> D2xxManager.FT_FLOW_NONE
        }

        // TODO : flow ctrl: XOFF/XOM
        // TODO : flow ctrl: XOFF/XOM
        ftDev!!.setFlowControl(flowCtrlSetting, 0x0b.toByte(), 0x0d.toByte())
        uart_configured = true
        //Toast.makeText(DeviceUARTContext, "Config done", Toast.LENGTH_SHORT).show();
    }

    // Start a coroutine to read messages in a non-blocking manner
    open fun startReadingMessages() = CoroutineScope(Dispatchers.IO).launch {
        messagesFlow = MutableSharedFlow<ByteArray>()

        while (isActive) {
            delay(50)
            var message = mutableListOf<Byte>()
            synchronized(ftDev!!) {
                iavailable = ftDev!!.queueStatus
                if (iavailable > 0) {
                    // Read the size of the message from the first byte
                    val sizeData = ByteArray(1)
                    ftDev!!.read(sizeData, 1)
                    val messageSize = sizeData[0].toInt()

                    // Make sure we have enough space in readData
                    if (messageSize > readData.size) {
                        readData = ByteArray(messageSize)
                        readDataToText = CharArray(messageSize)
                    }

                    // Read the message part by part
                    var bytesRead = 0
                    while (bytesRead < messageSize) {
                        val bytesToRead = min(messageSize - bytesRead, readLength)
                        bytesRead += ftDev!!.read(readData, bytesToRead)

                        // add the read data to messageData
                        for (i in 0 until bytesToRead) {
                            message.add(readData[i])
                        }
                    }

                }
            }
            if (message.size > 0) {
                messagesFlow?.emit(message.toByteArray())
            }
        }

    }

    open suspend fun readMessage(timeoutMillis: Long): ByteArray {
        //return withTimeout(timeoutMillis) {
            return messagesFlow!!.first()
        //}
    }

    open suspend fun sendMessage(message: ByteArray) {
        synchronized(ftDev!!) {
            if (!ftDev!!.isOpen) {
                Log.e("j2xx", "SendMessage: device not open")
                return
            }
            ftDev!!.latencyTimer = 16.toByte()

            // Ensure that the size of the message fits in one byte
            if (message.size > 255) {
                Log.e("j2xx", "SendMessage: message too large")
                return
            }
            val messageLength = message.size.toByte()
            val outData = byteArrayOf(messageLength) + message
            val ack = ByteArray(1)

            ftDev!!.write(outData, outData.size)

            ftDev!!.read(ack, 1)

            if (ack[0] != ACK) {
                throw Exception("Unack'ed message")
            }
        }
    }


}