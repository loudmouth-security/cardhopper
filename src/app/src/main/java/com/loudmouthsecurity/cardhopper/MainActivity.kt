package com.loudmouthsecurity.cardhopper

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.hardware.usb.UsbManager
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.net.toUri
import com.ftdi.j2xx.D2xxManager
import com.ftdi.j2xx.D2xxManager.D2xxException
import io.ktor.client.HttpClient
import io.ktor.client.engine.okhttp.OkHttp
import io.ktor.client.plugins.websocket.DefaultClientWebSocketSession
import io.ktor.client.plugins.websocket.WebSockets
import io.ktor.client.plugins.websocket.webSocket
import io.ktor.http.HttpMethod
import io.ktor.websocket.Frame
import io.ktor.websocket.readBytes
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.coroutines.cancellation.CancellationException


class MainActivity : AppCompatActivity() {

    private var ftD2xx: D2xxManager? = null
    private var ftdiHelper: FTDIHelper? = null
    private var statusText: TextView? = null
    private var debugText: TextView? = null
    private var goButton: Button? = null
    private var cancelButton: Button? = null
    private var modeSelect: Spinner? = null
    private var cardType: Spinner? = null
    private var job: Job? = null
    private var readerJob: Job? = null
    private var fwiLabel: TextView? = null
    private var fwiValue: TextView? = null
    private var sfgiLabel: TextView? = null
    private var sfgiValue: TextView? = null
    private var relayServer: TextView? = null


    private val CARD = "CARD".toByteArray()
    private val READER = "READ".toByteArray()
    private var RESTART = "RESTART".toByteArray()

    private val ERROR = "ff".toInt(16).toByte()
    private val SEOS = 12.toByte()
    private val EV1 = 3.toByte()

    private var FWI = "0e".toInt(16).toByte()
    private var SFGI = "00".toInt(16).toByte()


    // indexes in the string array
    private val CARD_INDEX = 0
    private val READER_INDEX = 1
    private val SEOS_INDEX = 0
    private val EV1_INDEX = 1

    private fun ByteArray.toHexString() = joinToString("") { "%02x".format(it) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Disable the go and cancel buttons to start
        goButton = findViewById<Button>(R.id.goButton)
        cancelButton = findViewById<Button>(R.id.cancelButton)
        goButton?.isEnabled = false
        cancelButton?.isEnabled = false
        val refreshButton = findViewById<Button>(R.id.refreshDevices)

        // Init the text fields
        statusText = findViewById<TextView>(R.id.statusText)
        debugText = findViewById<TextView>(R.id.debugText)
        relayServer = findViewById<TextView>(R.id.relayServer)
        fwiLabel = findViewById<TextView>(R.id.fwiLabel)
        fwiValue = findViewById<TextView>(R.id.fwi)
        sfgiLabel = findViewById<TextView>(R.id.sfgiLabel)
        sfgiValue = findViewById<TextView>(R.id.sfgi)

        debugText?.inputType = InputType.TYPE_NULL
        debugText?.isSingleLine = false

        // Set up the mode drop down
        modeSelect = findViewById<Spinner>(R.id.modeSelect)
        ArrayAdapter.createFromResource(
            this,
            R.array.mode,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            modeSelect?.adapter = adapter
        }

        // set up the FWI and SFGI visibility if Reader mode is chosen
        modeSelect?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                val selectedItem = parent.getItemAtPosition(position).toString()
                val readerMode = resources.getStringArray(R.array.mode)[READER_INDEX]

                if (selectedItem == readerMode) {
                    fwiLabel?.visibility = View.VISIBLE
                    fwiValue?.visibility = View.VISIBLE
                    sfgiLabel?.visibility = View.VISIBLE
                    sfgiValue?.visibility = View.VISIBLE
                } else {
                    fwiLabel?.visibility = View.INVISIBLE
                    fwiValue?.visibility = View.INVISIBLE
                    sfgiLabel?.visibility = View.INVISIBLE
                    sfgiValue?.visibility = View.INVISIBLE
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Do something when nothing is selected
            }

        }

        // Set up the card drop down
        cardType = findViewById<Spinner>(R.id.cardSelect)
        ArrayAdapter.createFromResource(
            this,
            R.array.card_type,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            cardType?.adapter = adapter
        }

        // Set up the FTDI Helper
        try {
            ftD2xx = D2xxManager.getInstance(this)
            if (!ftD2xx!!.setVIDPID(0x0403, 0xada1)) Log.i("ftd2xx-java", "setVIDPID Error")
        } catch (ex: D2xxException) {
            ex.printStackTrace()
        }
        ftdiHelper = FTDIHelper(applicationContext, ftD2xx!!)

        // Add an intent filter to capture device attach/detach
        val filter = IntentFilter()
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED)
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED)
        filter.priority = 500
        this.registerReceiver(mUsbReceiver, filter)

        // Set up the onclick listeners
        refreshButton.setOnClickListener{
            initFTDI()
        }

        goButton?.setOnClickListener {
            // check which mode we are configued for
            val mode = modeSelect?.selectedItem.toString()
            val card = cardType?.selectedItem.toString()

            goButton?.isEnabled = false
            cancelButton?.isEnabled = true
            debugText?.append("Let's go!\n")
            //debugText?.scrollTo(0, debugText!!.bottom)

            doRelay(mode, card)
        }

        cancelButton?.setOnClickListener {
            cancel("Cancelled!")
        }
    }

    private fun initFTDI() {
        ftdiHelper!!.init()
        if (ftdiHelper!!.isOpen()) {
            ftdiHelper!!.setConfig(112500, 8, 1, 0, 0 )
            goButton?.isEnabled = true

            statusText!!.text = "CardHopper connected and ready"
        } else {
            statusText!!.text = "Waiting for device..."
        }
    }

    private fun cancel(reason: String) {

        // try to interrupt the relay
        job?.cancel()
        job=null

        readerJob?.cancel()
        readerJob=null

        goButton?.isEnabled = true
        cancelButton?.isEnabled = false

        modeSelect?.isEnabled = true
        cardType?.isEnabled = true
        fwiValue?.isEnabled = true
        sfgiValue?.isEnabled = true

        debugText?.append(reason + "\n")
    }

    private fun doRelay(mode: String, card: String) {
        val client = HttpClient(OkHttp) {
            install(WebSockets)
        }

        modeSelect?.isEnabled = false
        cardType?.isEnabled = false
        fwiValue?.isEnabled = false
        sfgiValue?.isEnabled = false

        job = CoroutineScope(Dispatchers.IO).launch {
            try {
                readerJob = ftdiHelper!!.startReadingMessages()
                setupModeAndConfig(mode, card)

                startDataExchange(client, mode)
            } catch (e: Exception) {
                if (e != CancellationException())
                    writeDebugMessage("ERROR: $e\n")
                throw CancellationException("Error in coroutine", e)
            } finally {
                resetFTDI()
            }
        }
    }

    private suspend fun setupModeAndConfig(mode: String, card: String) {
        val cardMode = resources.getStringArray(R.array.mode)[CARD_INDEX]
        val readerMode = resources.getStringArray(R.array.mode)[READER_INDEX]
        val seosCardType = resources.getStringArray(R.array.card_type)[SEOS_INDEX]
        val desfireCardType = resources.getStringArray(R.array.card_type)[EV1_INDEX]

        when (mode) {
            cardMode -> {
                sendMessage(CARD)
                if (card == seosCardType) {
                    sendMessage(byteArrayOf(SEOS))
                } else if (card == desfireCardType) {
                    sendMessage(byteArrayOf(EV1))
                } else {
                    throw Exception("Non-implemented card type")
                }

                // get FWI and SFGI from UI
                FWI = fwiValue?.text.toString().toInt().toByte()
                if (FWI.toInt() > 14) {
                    FWI = 14.toByte()
                }

                SFGI = sfgiValue?.text.toString().toInt().toByte()
                if (SFGI.toInt() > 14) {
                    SFGI = 14.toByte()
                }
                sendMessage(byteArrayOf(FWI, SFGI))
            }
            readerMode -> {
                sendMessage(READER)
            }
            else -> {
                throw Exception("Non-implemented mode")
            }
        }
    }

    private suspend fun sendMessage(message: ByteArray) {
        ftdiHelper?.let { helper ->
            helper.sendMessage(message)
        } ?: throw Exception("ftdiHelper is not initialized")
    }

    private suspend fun readMessage() : ByteArray {
        ftdiHelper?.let { helper ->
           return helper.readMessage(helper.getReadTimeout())
        } ?: throw Exception("ftdiHelper is not initialized")
    }

    private suspend fun writeDebugMessage(message: String) {
        withContext(Dispatchers.Main) {
            debugText?.append(message)
        }
    }

    private suspend fun startDataExchange(client: HttpClient, mode: String) {
        val cardMode = resources.getStringArray(R.array.mode)[CARD_INDEX]
        val readerMode = resources.getStringArray(R.array.mode)[READER_INDEX]

        val uri = relayServer?.text.toString().toUri()

        client.webSocket(
            method = HttpMethod.Get,
            host = uri.host,
            port = uri.port,
            path = uri.path
        ) {
            try {
                if (mode == readerMode) {
                    exchangeReaderData(this)
                }
                if (mode == cardMode) {
                    exchangeCardData(this)
                }
                exchangeMessages(this)
            } catch (e: Exception) {
                throw Exception("Error in coroutine", e)
            } finally {
                // let the other side know we've errored if we can
                send(Frame.Binary(true, byteArrayOf(ERROR)))
            }
        }
    }

    private suspend fun exchangeReaderData(session: DefaultClientWebSocketSession) {
        val uid = readMessage()
        val ats = readMessage()

        writeDebugMessage("UID: ${uid.toHexString()}\n")
        writeDebugMessage("ATS: ${ats.toHexString()}\n")


        session.send(Frame.Binary(true, uid))
        session.send(Frame.Binary(true, ats))
    }

    private suspend fun exchangeCardData(session: DefaultClientWebSocketSession) {
        val uid = session.incoming.receive() as? Frame.Binary
        val uidBytes = uid!!.readBytes()

        if (uidBytes.equals(ERROR)) {
            throw Exception("Received Error message from other side.")
        }

        writeDebugMessage("Received UID: ${uidBytes.toHexString()}\n")
        sendMessage(uidBytes)

        val ats = session.incoming.receive() as? Frame.Binary
        val atsBytes = ats!!.readBytes()

        if (atsBytes.equals(ERROR)) {
            throw Exception("Received Error message from other side.")
        }

        writeDebugMessage("Received ATS: ${atsBytes.toHexString()}\n")
        sendMessage(atsBytes)

        val message = readMessage()
        writeDebugMessage("Sending: ${message.toHexString()}\n")
        session.send(Frame.Binary(true, message))

    }

    private suspend fun exchangeMessages(session: DefaultClientWebSocketSession) {
        while (true) {
            val message = session.incoming.receive() as? Frame.Binary
            val messageBytes = message!!.readBytes()
            writeDebugMessage("Received: ${messageBytes.toHexString()}\n")

            if (messageBytes.equals(ERROR)) {
                throw Exception("Received Error message from other side")
            }

            sendMessage(messageBytes)
            val myMessage = readMessage()

            writeDebugMessage("Sending: ${myMessage.toHexString()}\n")
            session.send(Frame.Binary(true, myMessage))
        }
    }

    private suspend fun resetFTDI() {
        try {
            for (i in 1..5) {
                ftdiHelper?.sendMessage(RESTART)
            }
        } catch (e: Exception) {
            writeDebugMessage("Error trying to reset device: ${e.toString()}")
        }
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        val action = intent.action
        if (UsbManager.ACTION_USB_DEVICE_ATTACHED == action) {
            ftdiHelper?.notifyUSBDeviceAttach()

            // cancel any current jobs
            cancel("Device disconnected!")

            statusText!!.text = "Waiting for device..."

            goButton?.isEnabled = false
            cancelButton?.isEnabled = false
        } else if (UsbManager.ACTION_USB_DEVICE_ATTACHED == action) {
            //initFTDI()
        }
    }

    /***********USB broadcast receiver */
    private val mUsbReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val tag = "FragL"
            val action = intent.action
            if (UsbManager.ACTION_USB_DEVICE_DETACHED == action) {
                Log.i(tag, "DETACHED...")

                ftdiHelper?.notifyUSBDeviceDetach()

                // cancel any current jobs
                cancel("Device disconnected!")

                statusText!!.text = "Waiting for device..."

                goButton?.isEnabled = false
                cancelButton?.isEnabled = false
            } else if (UsbManager.ACTION_USB_DEVICE_ATTACHED == action) {
                //initFTDI()
            }
        }
    }
}