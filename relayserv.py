import asyncio
import aiohttp
from aiohttp import web

# Global container for all WebSocket connections
sockets = set()

# HTTP route handler
async def root(request):
    return web.Response(text="Server is alive")

# WebSocket route handler
async def chat(request):
    ws_current = web.WebSocketResponse()
    await ws_current.prepare(request)

    sockets.add(ws_current)

    async for msg in ws_current:
        if msg.type == aiohttp.WSMsgType.TEXT:
            for ws_other in sockets:
                if ws_other is not ws_current:
                    await ws_other.send_str(msg.data)
        elif msg.type == aiohttp.WSMsgType.BINARY:
            for ws_other in sockets:
                if ws_other is not ws_current:
                    await ws_other.send_bytes(msg.data)
        elif msg.type == aiohttp.WSMsgType.ERROR:
            print('WebSocket connection closed with exception %s' % ws_current.exception())

    sockets.remove(ws_current)

    print('WebSocket connection closed')
    return ws_current

app = web.Application()
app.add_routes([web.get('/', root),
                web.get('/cardhopper', chat)])

web.run_app(app, host="0.0.0.0", port=6969)
