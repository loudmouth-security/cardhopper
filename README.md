# CardHopper

## Name
CardHopper - ISO14443-A long distance relay attack tools

## Description
This repository contains the Android APK for the CardHopper app associated with our DEF CON 31 talk - Unlocking Doors from half a continent away:

https://media.defcon.org/DEF%20CON%2031/DEF%20CON%2031%20presentations/Trevor%20Stevado%20Sam%20Haskins%20-%20Unlocking%20Doors%20from%20Half%20a%20Continent%20Away.pdf

CardHopper PCB schematic is also available to build compatible boards. 
Pre-built boards will be available for sale in the coming months - details to be posted here.

## Hardware
A limited number of Cardhopper PCBs are available for purchase.  Contact trevor@loudmouth.io for sales inquiries.

A compatible FFC cable is required to connect the PCB.  The recommended cable can be purchased at Mouser:
https://www.mouser.ca/ProductDetail/Wurth-Elektronik/687714050002?qs=qR1qlUC5%2FYRGvSHY4VDPRA%3D%3D

## Support
Please reach out to trevor@loudmouth.io with any questions or comments.

## Contributing
If you are interested in testing further distances with us, please reach out to the email above.

## License
CardHopper - ISO1443-A long distance relay attack tools
Copyright (C) 2023 Loudmouth Security Inc.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
